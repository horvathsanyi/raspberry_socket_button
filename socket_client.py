import socket

raspberry_ip = "192.168.0.101"


def receive_socket():
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((raspberry_ip, 5000))

        while True:
            print(s.recv(1024).decode("utf-8"))

    except Exception as e:
        print(f"An error occurs: {e}")


if __name__ == "__main__":
    receive_socket()

import socket
import RPi.GPIO as GPIO
import time

button_pin = 26
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)


def connect():
    s.bind(("", 5000))
    s.listen(1)
    print("Server is now listening!")


def setup_button():
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(button_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    return GPIO.HIGH


def background_controller(clientsocket, button_state):
    while True:
        current_state = GPIO.input(button_pin)
        # Check if the button is pressed
        if current_state == GPIO.LOW and button_state == GPIO.HIGH:
            message = "Hello from the other side!"
            print(message)
            clientsocket.send(bytes(message, "utf-8"))
        button_state = current_state
        time.sleep(0.1)


def main():
    connect()
    button_state = setup_button()
    while True:
        clientsocket, address = s.accept()
        print(f"Connection from ip {address} has been established!")
        background_controller(clientsocket, button_state)


if __name__ == "__main__":
    main()
